#!/bin/bash

SOURCE_YML=/nfs/cs-ccr-mon1/opt/cosmos/cosmos-config/setup/specific/prometheus-timservice.yml

SMITH_PATH=/opt/aps-mon/smith
GENERATOR_PATH=$SMITH_PATH/accsoft-smith-prometheus
PROM_PATH=$SMITH_PATH/prometheus
CONFIG_PATH=$PROM_PATH/config

python $GENERATOR_PATH/generatePrometheusConfig.py -s $SOURCE_YML -d $CONFIG_PATH/prometheus.next.yml -t $GENERATOR_PATH/template.yml 

if [ $? -ne 0 ]; then
  echo Configuration generation failed
  exit 0
fi

echo Configuration changes:
diff $CONFIG_PATH/prometheus.next.yml $CONFIG_PATH/prometheus.yml

if [ $? -eq 0 ]; then
  rm $CONFIG_PATH/prometheus.next.yml
  echo No change in config, nothing to do
  exit 0
fi

$PROM_PATH/promtool check config $CONFIG_PATH/prometheus.next.yml
if [ $? -ne 0 ]; then
  echo ERROR: Promtool check config failed.
  exit 1
fi

echo Installing new configuration and reloading prometheus...

mv $CONFIG_PATH/prometheus.next.yml $CONFIG_PATH/prometheus.yml
curl -X POST http://aps-prom:8000/-/reload
if [ $? -ne 0 ]; then
  echo ERROR: Failed to send reload command to prometheus.
  exit 1
fi

echo Prometheus configuration updated.
