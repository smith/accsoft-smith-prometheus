#!/usr/bin/python
import os
import argparse
import yaml
import sys
from shutil import copyfile
from datetime import datetime

def generateScrapeConfigs(file):
    jobConfig = list()
    with open(file, 'r') as stream:
        ymlContent = yaml.safe_load(stream)
        jobIndex = 0
        for node in ymlContent.items():
            infoMapVars = node[1]['list_mapvars'][0]
            infoPrometheus = node[1]['prometheus'][0]
            keyArgument, valueArgument = list(infoPrometheus['argument'].items())[0]

            jobConfig.append({
                    'job_name' : infoMapVars['application'],
                     'scrape_interval' : infoPrometheus['interval'],
                    'metrics_path' : '/cmx',
                    'params': {keyArgument: [valueArgument]},
                    'static_configs': []
                    })
            for host in node[1]['list_hosts']:
                jobConfig[jobIndex]['static_configs'].append({'targets': [host], 'labels': {'application': infoMapVars['application'], 'host' : host, "stage" : infoMapVars['stage']}})     
            jobIndex +=1
    return jobConfig

def generateTestConfig(host):
    return {
        'job_name' : 'cmx_test',
        'scrape_interval' : '60s',
        'metrics_path' : '/cmx',
        'params': {'component': ['test_module']},
        'static_configs': [{
            'targets': [host], 
            'labels': {'application': 'cmx_test', 'host' : host, "stage" : "test"}
            }]
    }

def parseTemplate(file):
    ymlContent = list()
    with open(file, 'r') as stream:
        ymlContent = yaml.safe_load(stream)

    return ymlContent
        
def generateConfig(sourceFile, destinationFile, templateFile):
    scrapeConfigsContent = generateScrapeConfigs(sourceFile)
    prometheusContent = parseTemplate(templateFile)
    prometheusContent['scrape_configs'] = scrapeConfigsContent
    prometheusContent['scrape_configs'].append(generateTestConfig('cwe-513-vol815'))

    with open(destinationFile, 'w') as outfile:
        yaml.dump(prometheusContent, outfile, default_flow_style=False)

def main():
    try:
        parser = argparse.ArgumentParser(description="Automatic Prometheus YAML configuration")
        parser.add_argument("-s", "--source",  action="store", dest="sourceFile", required=True, help="Source YAML configuration in COSMOS compatible format.")
        parser.add_argument("-d", "--destination",   action="store", dest="destinationFile", required=True, help="Destination YAML Prometheus configuration.")
        parser.add_argument("-t", "--template",   action="store", dest="templateFile", required=True, help="Prometheus configuration template.")
        
        args = parser.parse_args()
        arguments = vars(args)
        
        generateConfig(arguments['sourceFile'], arguments['destinationFile'], arguments['templateFile'])
    except Exception as e:
        print(e)
        sys.exit(1)
if __name__ == "__main__":
    main()
